use std::{collections::HashMap};

enum ClassifierError {
    InvalidInput,
    _OutOfRange,
    _InternalError,
}

pub struct KNeighborsClassifier {
    n_neighbors: usize,
    training_data: Vec<(String, Vec<f32>)>,
}

impl KNeighborsClassifier {
    pub fn new(k: usize, v: Vec<(String, Vec<f32>)>) -> Self {
        Self {
            n_neighbors: k,
            training_data: v,
        }
    }

    /// This function takes in a vector of vectors of floats, computes the distance between each vector
    /// and the stored patterns, and returns the most common label for each input vector.
    /// 
    /// Arguments:
    /// 
    /// * `x`: A vector of vectors of f32 values representing the input patterns to be classified.
    /// 
    /// Returns:
    /// 
    /// The `predict` function returns a vector of strings, which are the predicted class labels for the
    /// input patterns provided as a vector of vectors of f32 values.
    pub fn predict(&self, x: Vec<Vec<f32>>) -> Vec<String> {
        x.into_iter()
            .map(|pattern: Vec<f32>| {
                let distances: Vec<(String, f32)> = self.compute_all_distance(pattern);
                let mut recount: HashMap<String, i32> = HashMap::new();
                for (key, _) in distances {
                    *recount.entry(key).or_insert(0) += 1;
                }
                let mut sort_recount: Vec<(&String, &i32)> = recount.iter().collect();
                sort_recount.sort_by(|a, b| b.1.cmp(a.1));
                sort_recount[0].0.to_string()
            })
        .collect()
    }

    /// This function computes the distance between a pattern to classify and all training patterns, and
    /// returns a sorted list of the closest neighbors up to a specified number.
    /// 
    /// Arguments:
    /// 
    /// * `pattern_to_classify`: A vector of f32 values representing the pattern to be classified.
    /// 
    /// Returns:
    /// 
    /// The function `compute_all_distance` returns a vector of tuples, where each tuple contains a
    /// label (as a string) and its corresponding distance value (as a float) from the input
    /// `pattern_to_classify`. The vector is sorted in ascending order based on the distance values, and
    /// only the `n_neighbors` closest neighbors are returned.
    fn compute_all_distance(&self, pattern_to_classify: Vec<f32>) -> Vec<(String, f32)> {
        let mut ordered_list: Vec<(String, f32)> = Vec::new();
        self.training_data
            .iter()
            .for_each(|(label, training_pattern)| {
                if let Ok(value) = self.distance(&pattern_to_classify, training_pattern) {
                    ordered_list.push((label.to_string(), value))
                }
            });
        ordered_list.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
        ordered_list[..self.n_neighbors].to_vec()
    }

    /// This function calculates the Euclidean distance between two vectors of floating point numbers.
    /// 
    /// Arguments:
    /// 
    /// * `pattern_to_classify`: A vector of f32 values representing the pattern that needs to be
    /// classified.
    /// * `training_pattern`: A vector of f32 values representing a pattern from the training set.
    /// 
    /// Returns:
    /// 
    /// a `Result` enum with a value of either `Ok(f32)` if the calculation is successful or
    /// `Err(ClassifierError)` if the input is invalid.
    fn distance(&self,pattern_to_classify: &Vec<f32>,training_pattern: &Vec<f32>) -> Result<f32, ClassifierError> {
        if pattern_to_classify.len() != training_pattern.len() {
            return Err(ClassifierError::InvalidInput);
        }
        let mut sum: f32 = 0.0;
        training_pattern
            .iter()
            .enumerate()
            .for_each(|(index, value)| sum += (pattern_to_classify[index] - value).powf(2.0));
        Ok(sum.sqrt())
    }
}

fn main() {
    println!("Hello, world!");
}
