
use rand::{rngs::ThreadRng, Rng};
use std::{iter::zip, ops::Div};

pub struct KMeans {
    n_clusters: usize,
    centroids: Vec<Vec<f32>>,
    iterations: i16
}

impl KMeans {
    pub fn new(n_clusters: usize) -> Self {
        Self {
            n_clusters,
            centroids: Vec::new(),
            iterations: 10
        }
    }

    /// This function fits a K-means clustering model to a given dataset by iteratively updating the
    /// positions of the centroids.
    ///
    /// Arguments:
    ///
    /// * `x_train`: A reference to a vector of vectors of f32 values representing the training data.
    /// Each inner vector represents a data point with its features.
    pub fn fit(&mut self, x_train: &Vec<Vec<f32>>) {
        self.centroids.append(&mut self.randomly_start_centroids(x_train));
        for _ in 0..self.iterations { 
            self.update_centroids_position(&self.get_closet_centroids(x_train), x_train);
        }
    }

    /// This function predicts the cluster labels for a given set of test data points using the closest
    /// centroids.
    ///
    /// Arguments:
    ///
    /// * `x_test`: `x_test` is a vector of vectors of `f32` values representing the test data. Each
    /// inner vector represents a single data point with its features. The outer vector contains all the
    /// test data points. The function `predict` takes this test data as input and returns a vector of
    /// `usize
    ///
    /// Returns:
    ///
    /// The `predict` function returns a vector of `usize` values, which represent the indices of the
    /// closest centroids to the input vectors in `x_test`.
    pub fn predict(&self, x_test: Vec<Vec<f32>>) -> Vec<usize> {
        self.get_closet_centroids(&x_test)
            .iter()
            .map(|e: &(usize, f32)| e.0)
            .collect()
    }

    
    /// This function takes in a list of points and returns the closest centroids for each point.
    /// 
    /// Arguments:
    /// 
    /// * `points`: `points` is a slice of vectors, where each vector represents a point in a
    /// multi-dimensional space. The function `get_closet_centroids` takes this slice as input and
    /// returns a vector of tuples, where each tuple contains the index of the closest centroid and the
    /// distance between the point and
    /// 
    /// Returns:
    /// 
    /// A vector of tuples, where each tuple contains the index of the closest centroid and the distance
    /// to that centroid for each point in the input vector `points`.
    fn get_closet_centroids(&self, points: &[Vec<f32>]) -> Vec<(usize, f32)> {
        points
            .iter()
            .map(|points: &Vec<f32>| self.get_minimal_distance(points))
            .collect()
    }

    /// This function computes the minimal distance between a set of points and returns the index and
    /// value of the closest point.
    /// 
    /// Arguments:
    /// 
    /// * `points`: `points` is a vector of `f32` values representing the coordinates of points in a
    /// multi-dimensional space. The function `get_minimal_distance` takes this vector as input and
    /// computes the distances between each pair of points using the `compute_distances` method of the
    /// object `self`. It then
    fn get_minimal_distance(&self, points: &[f32]) -> (usize, f32) {
        self.compute_distances(points)
            .into_iter()
            .enumerate()
            .min_by(|(_, b1), (_, b2)| b1.total_cmp(b2))
            .unwrap()
    }

    /// This function computes the distances between centroids and points in a vector.
    ///
    /// Arguments:
    ///
    /// * `points`: A vector of f32 values representing the coordinates of a point in a
    /// multi-dimensional space. The number of dimensions is determined by the number of elements in the
    /// vector.
    ///
    /// Returns:
    ///
    /// The `compute_distances` function returns a vector of distances between each centroid in
    /// `self.centroids` and the given `points` vector.
    fn compute_distances(&self, points: &[f32]) -> Vec<f32> {
        self.centroids
            .iter()
            .map(|centroid| self.distance(centroid, points))
            .collect::<Vec<f32>>()
    }

    /// This function calculates the Euclidean distance between a centroid and a set of points in Rust.
    ///
    /// Arguments:
    ///
    /// * `centroid`: A vector of f32 values representing the centroid point in a multi-dimensional
    /// space.
    /// * `points`: `points` is a vector of `f32` values representing the coordinates of a set of points
    /// in a multi-dimensional space. The number of dimensions is determined by the length of the
    /// vector. For example, if `points` has length 3, then it represents a set of points in
    ///
    /// Returns:
    ///
    /// a single floating-point value, which represents the Euclidean distance between the centroid and
    /// the points.
    fn distance(&self, centroid: &Vec<f32>, points: &[f32]) -> f32 {
        points
            .iter()
            .zip(centroid)
            .fold(0.0, |acc, el| acc + (el.0 - el.1).powf(2.0))
            .sqrt()
    }

    /// This function updates the position of centroids based on the current cluster information and
    /// training data.
    ///
    /// Arguments:
    ///
    /// * `closet_centroids`: A vector of tuples containing information about the clusters. Each tuple
    /// contains the label of the cluster and the distance of the data point from its centroid.
    /// * `x_train`: The training data set, which is a vector of vectors where each inner vector
    /// represents a data point with its features.
    fn update_centroids_position(
        &mut self,
        closet_centroids: &Vec<(usize, f32)>,
        x_train: &Vec<Vec<f32>>,
    ) {
        for label in 0..self.centroids.len() {
            let points_filtered: Vec<&Vec<f32>> = zip(closet_centroids, x_train)
                .filter(|z: &(&(usize, f32), &Vec<f32>)| z.0 .0 == label)
                .map(|e: (&(usize, f32), &Vec<f32>)| e.1)
                .collect();

            self.centroids[label] = if points_filtered.is_empty() {
                self.randomly_start_centroids(x_train)[label].clone()
            } else {
                self.get_new_centroids(points_filtered, x_train[0].len())
            };

        }
    }

    /// The `get_new_centroids` function takes in a vector of references to vectors of `f32` values
    /// (`points`) and the number of dimensions (`points_dimensions`) and returns a new centroid vector.
    /// It calculates the mean of each dimension of the points in the cluster and returns a vector
    /// containing these means as its elements. It does this by first creating a vector of zeros with
    /// length equal to `points_dimensions` using `vec![0.0; points_dimensions]`. It then iterates over
    /// the dimensions of the points vector using `enumerate()` and for each dimension, it calculates
    /// the mean of that dimension by summing the values of that dimension for all points in the cluster
    /// and dividing by the number of points in the cluster. Finally, it collects these means into a new
    /// vector and returns it.
    fn get_new_centroids(&self, points: Vec<&Vec<f32>>, points_dimensions: usize) -> Vec<f32> {
        vec![0.0; points_dimensions]
            .iter()
            .enumerate()
            .map(|(dim, _)| {
                return points
                    .iter()
                    .fold(0.0, |acc, &el| acc + el[dim])
                    .div(points.len() as f32);
            })
            .collect()
    }

    /// This function generates random starting centroids for k-means clustering algorithm.
    ///
    /// Arguments:
    ///
    /// * `x`: A reference to a vector of vectors of f32 values, representing the dataset on which the
    /// K-means algorithm will be applied. Each inner vector represents a data point with multiple
    /// features.
    ///
    /// Returns:
    ///
    /// a vector of vectors, where each inner vector represents the coordinates of a randomly generated
    /// centroid for a cluster. The number of centroids returned is equal to the value of
    /// `self.n_clusters`.
    fn randomly_start_centroids(&self, x: &[Vec<f32>]) -> Vec<Vec<f32>> {
        let (min, max) = self.get_min_max(x);
        let mut rng: ThreadRng = rand::thread_rng();
        (0..self.n_clusters)
            .map(|_| {
                vec![0.0; x[0].len()]
                    .iter()
                    .map(|_| rng.gen_range(min..max))
                    .collect()
            })
            .collect::<Vec<Vec<f32>>>()
    }

    /// This function takes a 2D vector of f32 values and returns a tuple containing the minimum and
    /// maximum values in the vector.
    ///
    /// Arguments:
    ///
    /// * `x`: A two-dimensional vector of type `Vec<Vec<f32>>` containing floating-point numbers. This
    /// function returns a tuple containing the minimum and maximum values found in the input vector.
    fn get_min_max(&self, x: &[Vec<f32>]) -> (f32, f32) {
        let mut min: f32 = x[0][0];
        let mut max: f32 = x[0][0];
        for val in x.iter().flatten() {
            if val > &max { max = *val }
            if val < &min { min = *val }
        }
        (min, max)
    }
}

fn main() {
    println!("Hello, world!");
}
