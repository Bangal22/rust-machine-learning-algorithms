use rand::{rngs::ThreadRng, Rng};
use std::iter::zip;
use std::ops::{Add, Div, Mul, Sub};

pub struct MultipleLinearRegression {
    weights: Vec<f32>,
    bias_coefficient: f32,
    iterations: usize,
    learning_rate: f32,
}

impl Default for MultipleLinearRegression {
    fn default() -> Self {
        Self {
            weights: Vec::new(),
            bias_coefficient: 0.0,
            iterations: 1000,
            learning_rate: 0.01,
        }
    }
}

impl MultipleLinearRegression {
    pub fn _new(iterations: usize, learning_rate: f32) -> MultipleLinearRegression {
        MultipleLinearRegression {
            weights: Vec::new(),
            bias_coefficient: 0.0,
            iterations,
            learning_rate,
        }
    }

    pub fn fit(&mut self, x_train: &Vec<Vec<f32>>, y_train: &Vec<f32>) {
        self.weights = self.randomly_weights(x_train[0].len());
        self.gradient_descent(x_train, y_train);
    }


    fn gradient_descent(&self, x_train: &Vec<Vec<f32>>, y_train: &Vec<f32>) -> f32 {
        for _ in 0..self.iterations {
            let y_pred: Vec<f32> = self
                .dot(&self.weights, x_train)
                .iter()
                .map(|y: &f32| y.add(self.bias_coefficient))
                .collect();

            let weight_derivate = todo!();
            let bias_derivate = todo!();
        }
        0.0
    }

    pub fn dot(&self, weights: &Vec<f32>, x_train: &Vec<Vec<f32>>) -> Vec<f32> {
        match weights.len() == x_train[0].len() {
            true => self
                .transpose(x_train)
                .iter()
                .map(|x_values: &Vec<f32>| {
                    zip(weights, x_values).fold(0.0, |acc, (w, x)| acc + (w * x))
                })
                .collect(),
            false => panic!("Dimension error"),
        }
    }

    fn randomly_weights(&self, range: usize) -> Vec<f32> {
        let mut rng: ThreadRng = rand::thread_rng();
        (0..range).map(|_| rng.gen_range(0.0..1.0)).collect()
    }

    fn transpose(&self, matrix: &Vec<Vec<f32>>) -> Vec<Vec<f32>> {
        let num_rows: usize = matrix.len();
        let num_cols: usize = matrix[0].len();

        let mut transposed: Vec<Vec<f32>> = vec![vec![0.0; num_rows]; num_cols];

        for i in 0..num_rows {
            for j in 0..num_cols {
                transposed[j][i] = matrix[i][j]
            }
        }

        transposed
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_dot() {
        let mlr: MultipleLinearRegression = MultipleLinearRegression::default();
        assert_eq!(
            mlr.dot(
                &vec![1.0, 2.0, 3.0],
                &vec![vec![10.0, 11.0], vec![20.0, 21.0], vec![30.0, 31.0]]
            ),
            vec![140.0, 146.0]
        );
        assert_eq!(
            mlr.dot(
                &vec![2.0, 0.0, 1.0],
                &vec![
                    vec![1.0, 0.0, 1.0],
                    vec![1.0, 2.0, 1.0],
                    vec![1.0, 1.0, 0.0]
                ]
            ),
            vec![3.0, 1.0, 2.0]
        )
    }

    #[test]
    fn test_transpose() {
        let mlr: MultipleLinearRegression = MultipleLinearRegression::default();
        assert_eq!(
            mlr.transpose(&vec![vec![1.0, 2.0, 3.0], vec![4.0, 5.0, 6.0], vec![7.0, 8.0, 9.0]]),
            vec![
                vec![1.0, 4.0, 7.0],
                vec![2.0, 5.0, 8.0],
                vec![3.0, 6.0, 9.0]
            ]
        )
    }
}

fn main() {
    println!("Hello, world!");
}
