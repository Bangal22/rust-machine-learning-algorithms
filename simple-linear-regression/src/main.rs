use std::ops::{Add, Div, Mul, Sub};

pub struct LinearRegression {
    scale_factor: f32,     // (Σ(Xi - X̄)(Yi - Ȳ)) / (Σ(Xi - X̄)²)
    bias_coefficient: f32, // a = Ȳ - bX̄
}

impl LinearRegression {
    pub fn new() -> LinearRegression {
        LinearRegression {
            scale_factor: 0.0,
            bias_coefficient: 0.0,
        }
    }

    /// This function fits a linear regression model to the given training data.
    ///
    /// Arguments:
    ///
    /// * `x_train`: A vector of f32 values representing the input features of the training data.
    /// * `y_train`: The y_train parameter is a reference to a vector of f32 values representing the
    /// target variable values for the training data. In other words, it contains the correct output
    /// values for the corresponding input values in the x_train vector.
    pub fn fit(&mut self, x_train: &Vec<f32>, y_train: &Vec<f32>) {
        let (x_mean, y_mean) = (self.get_mean(x_train), self.get_mean(y_train));
        self.scale_factor = self.get_scale_factor(x_train, y_train, &x_mean, &y_mean);
        self.bias_coefficient = self.get_bias_coefficient(&x_mean, &y_mean);
    }

    /// This function takes a vector of float values, scales them by a factor and adds a bias
    /// coefficient, and returns the resulting vector.
    ///
    /// Formula:
    ///
    /// 𝑌 = m ∙ 𝑋1 + b
    ///
    /// Arguments:
    ///
    /// * `y_test`: A vector of f32 values representing the true values of the target variable in a
    /// machine learning model. This function takes in the y_test vector and applies a scaling factor
    /// and bias coefficient to each element, returning a new vector of predicted values.
    ///
    /// Returns:
    ///
    /// The `predict` function returns a `Vec<f32>` which is the result of scaling and biasing each
    /// element of the input `y_test` vector using the `scale_factor` and `bias_coefficient` values
    /// stored in the `self` object.
    pub fn predict(&self, x_test: &[f32]) -> Vec<f32> {
        x_test
            .iter()
            .map(|e: &f32| self.scale_factor.mul(e).add(self.bias_coefficient))
            .collect::<Vec<f32>>()
    }

    /// This function calculates the scale factor for linear regression given training data and their
    /// means.
    ///
    /// Formula:
    ///
    /// m = (Σ(Xi - X̄)(Yi - Ȳ)) / (Σ(Xi - X̄)²)
    ///
    /// Arguments:
    ///
    /// * `x_train`: A vector of f32 values representing the input features of a training dataset.
    /// * `y_train`: The vector of target values for the training data.
    /// * `x_mean`: The mean value of the x_train vector.
    /// * `y_mean`: The mean value of the y_train vector.
    ///
    /// Returns:
    ///
    /// a single `f32` value, which is the scale factor calculated using the provided input parameters.
    fn get_scale_factor(
        &self,
        x_train: &[f32],
        y_train: &Vec<f32>,
        x_mean: &f32,
        y_mean: &f32,
    ) -> f32 {
        x_train
            .iter()
            .zip(y_train)
            .fold(0.0, |acc: f32, el: (&f32, &f32)| {
                acc.add(el.0.sub(x_mean)).mul(el.1.sub(y_mean))
            })
            .div(
                x_train
                    .iter()
                    .fold(0.0, |acc: f32, el: &f32| acc.add(el.sub(x_mean)).powf(2.0)),
            )
    }

    /// This function calculates the bias coefficient for a linear regression model.
    ///
    /// Formula:
    ///
    /// b = Ȳ - bX̄
    ///
    /// Arguments:
    ///
    /// * `x_mean`: The mean value of the input variable x.
    /// * `y_mean`: The average value of the dependent variable (y) in a dataset.
    ///
    /// Returns:
    ///
    /// a `f32` value which represents the bias coefficient calculated using the provided `x_mean` and
    /// `y_mean` values.
    fn get_bias_coefficient(&self, x_mean: &f32, y_mean: &f32) -> f32 {
        y_mean.sub(self.scale_factor.mul(x_mean))
    }

    /// This function calculates the mean of a vector of floating point numbers.
    ///
    /// Arguments:
    ///
    /// * `points`: `points` is a vector of `f32` values representing a set of numerical data points.
    /// The function `get_mean` takes this vector as an input parameter and calculates the mean
    /// (average) of the values in the vector.
    ///
    /// Returns:
    ///
    /// The function `get_mean` returns the mean (average) value of a vector of `f32` points.
    fn get_mean(&self, points: &Vec<f32>) -> f32 {
        points.iter().sum::<f32>().div(points.len() as f32)
    }

    /// This function calculates the coefficient of determination (R-squared) for a regression model.
    ///
    /// Formula:
    ///
    /// 𝑹𝟐 = 1 - (∑(𝜸𝒊 − 𝜸𝒊̂ )𝟐) / (∑(𝜸𝒊 − 𝒚̅ )𝟐)
    ///
    /// Arguments:
    ///
    /// * `x_test`: A vector of input features used for testing the model's performance.
    /// * `y_test`: The actual target values for the test set.
    ///
    /// Returns:
    ///
    /// a single value of type `f32`, which represents the R-squared value of the model's predictions on
    /// the given test data.
    pub fn r_square(&self, x_test: &[f32], y_test: &Vec<f32>) -> f32 {
        let y_pred: Vec<f32> = self.predict(x_test);
        let y_mean: f32 = self.get_mean(y_test);
        1.0 - y_test
            .iter()
            .zip(y_pred)
            .fold(0.0, |acc: f32, el: (&f32, f32)| {
                acc.add(el.0.sub(el.1)).powf(2.0)
            })
            .div(
                x_test
                    .iter()
                    .fold(0.0, |acc: f32, el: &f32| acc.add(el.sub(y_mean)).powf(2.0)),
            )
    }
}

fn main() {
    println!("Hello, world!");
}
